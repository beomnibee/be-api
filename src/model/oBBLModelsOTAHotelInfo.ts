/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OTABreakfastHours } from './oBBLModelsOTABreakfastHours';
import { OTACategoryCodes } from './oBBLModelsOTACategoryCodes';
import { OTACheckInHours } from './oBBLModelsOTACheckInHours';
import { OTAClosedSeasonsType } from './oBBLModelsOTAClosedSeasonsType';
import { OTADescriptions } from './oBBLModelsOTADescriptions';
import { OTAHotelAmenityType } from './oBBLModelsOTAHotelAmenityType';
import { OTAOwnershipManagementInfosType } from './oBBLModelsOTAOwnershipManagementInfosType';
import { OTAPositionType } from './oBBLModelsOTAPositionType';
import { OTAReceptionHours } from './oBBLModelsOTAReceptionHours';


export interface OTAHotelInfo {
    WhenBuilt?: string;
    NFloors?: number;
    HandicapRooms?: number;
    SmokingRooms?: number;
    NonSmokingHotel?: boolean;
    CheckInHours?: OTACheckInHours;
    ReceptionHours?: OTAReceptionHours;
    BreakfastHours?: OTABreakfastHours;
    AdditionalWorkingHours?: string;
    ClosedSeasonsType?: OTAClosedSeasonsType;
    Descriptions?: OTADescriptions;
    CategoryCodes?: OTACategoryCodes;
    Position?: OTAPositionType;
    HotelAmenities?: Array<OTAHotelAmenityType>;
    OwnershipManagementInfosType?: OTAOwnershipManagementInfosType;
    TotalRooms?: string;
}
