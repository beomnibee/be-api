/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface OTAPaymentCardType {
    EffectiveDate?: Date;

    ExpireDate?: Date;

    CardCode?: OTAPaymentCardType.CardCodeEnum;

    CardHolderName?: string;

    CardNumber?: string;

    SeriesCode?: string;

}
export namespace OTAPaymentCardType {
    export enum CardCodeEnum {
        NUMBER_1 = <any> 1,
        NUMBER_2 = <any> 2,
        NUMBER_3 = <any> 3,
        NUMBER_4 = <any> 4,
        NUMBER_5 = <any> 5,
        NUMBER_6 = <any> 6,
        NUMBER_7 = <any> 7,
        NUMBER_8 = <any> 8,
        NUMBER_9 = <any> 9,
        NUMBER_10 = <any> 10,
        NUMBER_11 = <any> 11,
        NUMBER_12 = <any> 12,
        NUMBER_13 = <any> 13,
        NUMBER_14 = <any> 14,
        NUMBER_15 = <any> 15,
        NUMBER_16 = <any> 16
    }
}
