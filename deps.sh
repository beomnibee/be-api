unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     sh .deps/linux.sh $1;;
    Darwin*)    sh .deps/mac.sh $1;;
    CYGWIN*)    sh .deps/linux.sh $1;;
    MINGW*)     sh .deps/linux.sh $1;;
    *)          echo 'Machine Unknown or not Supported'
esac
